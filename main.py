import PySimpleGUI as psg
import random

# Wizualizacja sortowania bąbelkowego poprzez kolumny imitujące listy danych
BAR_SPACING, BAR_WIDTH, EDGE_OFFSET = 11, 10, 3
DATA_SIZE = GRAPH_SIZE = (700, 500)      # szerokość, wysokość danych grafu


def sortowanie_babelkowe(arr):
    def swap(i, j):
        arr[i], arr[j] = arr[j], arr[i]
    n = len(arr)
    swapped = True
    x = -1
    while swapped:
        swapped = False
        x = x + 1
        for i in range(1, n - x):
            if arr[i - 1] > arr[i]:
                swap(i - 1, i)
                swapped = True
                yield arr


def draw_bars(graph, items):
    for i, item in enumerate(items):
        graph.draw_rectangle(top_left=(i * BAR_SPACING + EDGE_OFFSET, item),
                             bottom_right=(i * BAR_SPACING +
                                           EDGE_OFFSET + BAR_WIDTH, 0),
                             fill_color='#67b836') # kolor słupków


def main():
    psg.theme('LightGreen')
    # Lista do sortowania
    num_bars = DATA_SIZE[0] // (BAR_WIDTH + 1)
    list_to_sort = [DATA_SIZE[1] // num_bars * i for i in range(1, num_bars)]
    random.shuffle(list_to_sort)

    # Definiowanie okna
    graph = psg.Graph(GRAPH_SIZE, (0, 0), DATA_SIZE)
    layout = [[graph],
              [psg.Text('Prędkość    Szybciej'), psg.Slider((0, 20), orientation='h', default_value=10, key='-SPEED-'), psg.Text('Wolniej')]]

    window = psg.Window('Wizualizacja sortowania bąbelkowego',
                       layout, finalize=True)
    # Rysowanie inicjalizacji okna
    draw_bars(graph, list_to_sort)

    # Sortowanie zaczyna się po naciśnięciu "START"
    psg.popup('Naciśnij START, żeby zacząć wizualizację')
    bsort = sortowanie_babelkowe(list_to_sort)
    timeout = 10  # zacznij z 10ms opóźnieniem
    while True:
        event, values = window.read(timeout=timeout)
        if event == psg.WIN_CLOSED:
            break
        try:
            partially_sorted_list = bsort.__next__()
        except:
            psg.popup('Sortowanie zakończone!')
            break
        graph.erase()
        draw_bars(graph, partially_sorted_list)
        timeout = int(values['-SPEED-'])

    window.close()


main()
